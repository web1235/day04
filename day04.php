<!DOCTYPE html> 
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>SignUp</title>
<body>

<center> 

    <fieldset style='width: 500px; height: 500px; border:#5b9bd5 solid'>
    <?php
        $name = $gender = $faculty = $birthOfDate = $address= "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(empty(inputHandling($_POST["name"]))){
                echo "<div style='color: red; text-align: left; '>Hãy nhập tên</div>";
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red; text-align: left;'>Hãy chọn giới tính</div>";
            }
            if(empty(inputHandling($_POST["faculty"]))){
                echo "<div style='color: red; text-align: left;'>Hãy chọn phân khoa</div>";
            }

            if(empty(inputHandling($_POST["birthOfDate"]))){
                echo "<div style='color: red; text-align: left;'>Hãy nhập ngày sinh</div>";
            }
            elseif (!validateDate($_POST["birthOfDate"])) {
                echo "<div style='color: red; text-align: left;'>Hãy nhập ngày sinh đúng định dạng</div>";
            }
    
        }

        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validateDate($date){
            if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',$date)) {
                return true;
            } else {
                return false;
            }

        }
    ?>
    <form style='margin: 20px 50px 0 35px' method="post">
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #5b9bd5; 
                text-align: left; padding: 5px 5px'>
                    <label style='color: white; font-size: 20px;'>Họ và tên</label>
                    <span class="error" style = 'color: red; font-size: 15px;'>*</span>
                </td>
                <td width = 30%   >
                    <input type='text' name = "name" style = 'line-height: 32px; border-color:#5b9bd5'>
                    
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 30% style = 'background-color:#5b9bd5; text-align: left; padding: 5px 5px'>
                    <label style='color: white; font-size: 20px;'>Giới tính</label>
                    <span class="error" style = 'color: red; font-size: 15px;'>*</span>

                </td>
                <td width = 30% >
                <?php
                    $genderArr=array("Nam","Nữ");
                    for($x = 0; $x < count($genderArr); $x++){
                        echo"
                            <label class='container'>
                                <input type='radio' value=".$genderArr[$x]." name='gender'>"
                                .$genderArr[$x]. 
                            "</label>";
                    }
                ?>  

                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #5b9bd5; text-align: left; padding: 5px 5px'>
                    <label style='color: white;font-size: 20px;'>Phân Khoa</label>
                    <span class="error" style = 'color: red; font-size: 15px;'>*</span>

                </td>
                <td height = '40px'>
                    <select name='faculty' style = 'border-color:#5b9bd5;height: 100%;width: 80%;'>
                        <?php
                            $facultyArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($facultyArr as $x=>$x_value){
                                echo"<option>".$x_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr height = '40px'>
                <td style = 'background-color:#5b9bd5;  text-align: left; padding: 5px 5px'>
                    <label style='color: white;font-size: 20px;'>Ngày sinh</label>
                    <span class="error" style = 'color: red; font-size: 15px;'>*</span>

                </td>
                <td height = '40px'>
                    <input type='date' name="birthOfDate" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#5b9bd5'>
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #5b9bd5; text-align: left; padding: 5px 5px'>
                    <label style='color: white; font-size: 20px;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <input type='text' name="address" style = 'line-height: 32px; border-color:#5b9bd5'> 
                </td>
            </tr>

        </table>
    <button style='background-color: #70ad47; font-family: none; font-size: 20px; border-radius: 10px; width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng ký</button>
    </form>

</fieldset>
</center>
</body>
</html>


